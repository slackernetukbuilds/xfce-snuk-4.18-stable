# This project based on [XFCE](https://www.xfce.org/) &  [Slackware](http://www.slackware.com/) 

### Configuring

For xfce4-settings - [Color Profiles](https://docs.xfce.org/xfce/xfce4-settings/color) we need to add the colord group/user.
In console (root), type:
```bash
groupadd -g 303 colord
useradd -d /var/lib/colord -u 303 -g colord -s /bin/false colord
```


# Installing


**Building Instructions**

If you would like to install XFCE with Kvantum, we need to install some KDE dependencies:<br><br>
`KDE=yes build.sh`

**_Slackware 15.0_**

```
$ git clone https://gitlab.com/slackernetukbuilds/xfce-snuk-4.18-stable.git 
$ cd xfce-snuk-4.18-stabe 
# sh build.sh
```


**Configuration**



Add to /etc/slackpkg/blacklist file

`[0-9]+_snuk`

`xwmconfig` to setup XFCE as standard-session
