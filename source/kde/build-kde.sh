#!/bin/bash

for i in \
    extra-cmake-modules \
    solid \
    breeze-icons \
    kidletime \
    plasma-wayland-protocols \
    kwayland \
    libkscreen \
    kwindowsystem \
; do
cd ${i} || exit 1
sh ${i}.SlackBuild || exit 1
cd ..
done
