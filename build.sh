#!binbash

KDE=${KDE:-no}


kdeps=(
        extra-cmake-modules 
        solid 
        breeze-icons 
        kidletime 
        plasma-wayland-protocols 
        kwayland 
        libkscreen 
        kwindowsystem 
        Kvantum
)	


core=(
	libopenraw 
	libgusb 
	gnome-common 
	python3-webencodings 
	html5lib 
	python-toml 
	python2-setuptools-scm 
	functools-lru-cache 
	python2-soupsieve 
	python2-BeautifulSoup4 
	python3-soupsieve 
	BeautifulSoup4 
	lxml 
	bamf 
	colord 
	cogl 
	clutter 
	xfce4-dev-tools 
	libxfce4util 
	xfconf 
	libxfce4ui 
	exo 
	garcon 
	xfce4-panel 
	thunar 
	thunar-volman 
	tumbler 
	xfce4-appfinder 
	xfce4-power-manager 
	xfce4-settings 
	xfdesktop 
	xfwm4 
	xfce4-session 
	xfce4-terminal 
	xfce4-notifyd 
	xfdashboard 
	xfce4-pulseaudio-plugin 
	vala-panel-appmenu 
	xfce4-whiskermenu-plugin 
	xfce4-screensaver 
	libsoup3 
	xfce4-screenshooter 
	xfce4-mailwatch-plugin 
	xfce4-weather-plugin 
	ristretto 
	mousepad 
	xiccd 
	cantarell-fonts 
	elementary-xfce 
	gnome-icon-theme 
	gnome-icon-theme-extras 
	gnome-menus 
	plank 
	panther_launcher
	breezex-cursor-theme	
)	

if [ "${KDE:-no}" = "yes" ]; then
        for k in ${kdeps[@]}; do 
        cd source/kde/${k} || exit 1
        sh ${k}.SlackBuild || exit 1
        cd ../../.. 
        done
fi

        for c in ${core[@]}; do 
        cd source/${c} || exit 1
        sh ${c}.SlackBuild || exit 1
        cd ../..
        done

